package com.atlassian.annotations.nullability;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.annotation.meta.TypeQualifierDefault;

import org.checkerframework.checker.nullness.qual.NonNull;

/**
 * Similar to {@link javax.annotation.ParametersAreNonnullByDefault}, but based on Checker Framework.
 *
 * @since 2.2.0
 */
@Documented
@NonNull
@TypeQualifierDefault(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface ParametersAreNonnullByDefault {}
