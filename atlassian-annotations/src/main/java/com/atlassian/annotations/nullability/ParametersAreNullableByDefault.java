package com.atlassian.annotations.nullability;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.annotation.meta.TypeQualifierDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * Similar to {@link javax.annotation.ParametersAreNullableByDefault}, but based on Checker Framework.
 *
 * @since 2.2.0
 */
@Documented
@Nullable
@TypeQualifierDefault(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface ParametersAreNullableByDefault {}
