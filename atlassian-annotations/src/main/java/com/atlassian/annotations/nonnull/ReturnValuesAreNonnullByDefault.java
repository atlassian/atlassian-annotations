package com.atlassian.annotations.nonnull;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierDefault;

/**
 * Similar to {@link javax.annotation.ParametersAreNonnullByDefault}, but applies to return values of methods.
 *
 * @deprecated since 2.2.0. Use corresponding Checker Framework based annotation {@link com.atlassian.annotations.nullability.ReturnValuesAreNonnullByDefault}
 */
@Documented
@Nonnull
@TypeQualifierDefault({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Deprecated
public @interface ReturnValuesAreNonnullByDefault {}
