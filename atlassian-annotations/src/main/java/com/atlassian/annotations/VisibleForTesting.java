package com.atlassian.annotations;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.CLASS;

/**
 * Annotates a program element that exists, or is more widely visible than
 * otherwise necessary, only for use in test code.
 */
@Retention(CLASS)
public @interface VisibleForTesting {}
