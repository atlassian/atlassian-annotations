package com.atlassian.annotations.security;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotated element is excluded from
 * Cross-site request forgery (CSRF/XSRF) protection.
 * <p>
 * This element is designed for products and plugins to
 * <em>exclude</em> resource methods from XSRF protection.
 * </p>
 *
 * @since 0.9
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.METHOD)
public @interface XsrfProtectionExcluded {}
