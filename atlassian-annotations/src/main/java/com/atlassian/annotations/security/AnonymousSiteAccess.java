package com.atlassian.annotations.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Permits resource access if at least ONE of the following criteria is met:</p>
 * <ul>
 * <li>Current user is unauthenticated AND anonymous access enabled for site (see {@link UserManager#isAnonymousAccessEnabled})</li>
 * <li>Current user is authenticated AND limited unlicensed access enabled for site (see {@link UserManager#isLimitedUnlicensedAccessEnabled})</li>
 * <li>Current user is authenticated AND assigned a product license (see {@link UserManager#isLicensed})</li>
 * </ul>
 *
 * @since 5.0.0
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AnonymousSiteAccess {}
