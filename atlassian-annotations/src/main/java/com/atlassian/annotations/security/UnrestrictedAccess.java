package com.atlassian.annotations.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This annotation permits complete unrestricted access. It will permit unauthenticated access irrespective of
 * whether the site has enabled anonymous access.</p>
 * <p>Please use with care and consider if {@link AnonymousSiteAccess}, which only permits access to anonymous users
 * when site has anonymous access enabled, is suitable for your needs.</p>
 *
 * Please note that this annotation is not inherited.
 *
 * @see AnonymousSiteAccess
 * @since 5.0.0
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UnrestrictedAccess {}
