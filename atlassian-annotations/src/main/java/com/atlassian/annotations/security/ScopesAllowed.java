package com.atlassian.annotations.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.atlassian.annotations.ExperimentalApi;

/**
 * <p>This annotation is part of the experimental API. It is likely to change
 * in the near future, and it is not ready for vendor usage.</p>
 *
 * <p>Permits resource access to service accounts</p>
 *
 * @since 5.1.0
 */
@ExperimentalApi
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ScopesAllowed {
    String[] requiredScope();
}
