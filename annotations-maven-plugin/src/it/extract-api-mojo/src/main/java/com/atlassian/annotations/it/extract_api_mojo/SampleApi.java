package com.atlassian.annotations.it.extract_api_mojo;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.annotations.PublicApi;
import com.atlassian.annotations.PublicSpi;

import java.util.List;

public interface SampleApi {
    @PublicApi
    void publicApiMethod1();

    @PublicSpi
    int publicSpiMethod1(String arg1);

    @ExperimentalApi
    String experimentalApiMethod1(int arg1, String arg2, List<String> arg3);

    @ExperimentalSpi
    List<String> experimentalSpiMethod1(String ... arg1);
}