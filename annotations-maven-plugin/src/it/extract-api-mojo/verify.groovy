import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

/**
 * input param passed by maven-invoker-plugin
 * @see <a href="https://maven.apache.org/plugins/maven-invoker-plugin/examples/post-build-script.html">Maven Invoker Plugin</a>
 */
Path basedirPath = basedir.toPath()

def extractApiOutputPath = basedirPath.resolve(Paths.get("target", "extract-api.xml"))
def extractApiExpectedOutput = basedirPath.resolve(Paths.get("src","test", "resources", "expected-extract-api.xml"))

def extractApiOutput = Files.readAllLines(extractApiOutputPath)
def extractApiExpected = Files.readAllLines(extractApiExpectedOutput)

return extractApiOutput.equals(extractApiExpected)