package com.atlassian.annotations;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.metadata.ArtifactMetadataSource;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.resolver.filter.ArtifactFilter;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.InvalidProjectModelException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;

public abstract class AbstractAnnotationsMojo extends AbstractMojo {

    /**
     * @parameter property="project"
     * @required
     */
    protected MavenProject project;

    /**
     * @component
     */
    protected ArtifactFactory artifactFactory;

    /**
     * All projects listed in the reactor
     *
     * @parameter property="reactorProjects"
     * @required
     */
    protected List<MavenProject> reactorProjects;

    /**
     * @component
     */
    protected MavenProjectBuilder projectBuilder;

    /**
     * @component
     */
    protected ArtifactResolver resolver;

    /**
     * @parameter default-value="${localRepository}"
     * @required
     * @readonly
     */
    protected ArtifactRepository localRepository;

    /**
     * @parameter default-value="${project.remoteArtifactRepositories}"
     * @required
     * @readonly
     */
    protected List remoteRepositories;

    /**
     * @component
     */
    protected ArtifactMetadataSource metadataSource;

    protected PublicApiAnalyser getPublicApiAnalyser() throws MojoExecutionException {
        final Set<URL> urlsToReactorProjects = getArtifactsFromReactorAsURLs();

        final Set<URL> classPathUrls = getAllDependenciesAsURLs();

        PublicApiAnalyser publicApiAnalyser;
        try {
            publicApiAnalyser = new PublicApiAnalyser(urlsToReactorProjects, classPathUrls);
        } catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        return publicApiAnalyser;
    }

    private Set<URL> getAllDependenciesAsURLs() throws MojoExecutionException {
        return getAllDependencies().stream().map(this::artifactToUrl).collect(Collectors.toCollection(HashSet::new));
    }

    private Set<URL> getArtifactsFromReactorAsURLs() {
        return getArtifactsFromReactor().stream()
                .map(this::artifactToUrl)
                .collect(Collectors.toCollection(HashSet::new));
    }

    private Set<Artifact> getAllDependencies() throws MojoExecutionException {
        final Set<Artifact> projects = getReactorProjectDependencies();

        if (projects == null) {
            throw new MojoExecutionException("Could not resolve dependent projects");
        }

        if (reactorProjects == null || reactorProjects.isEmpty()) {
            throw new MojoExecutionException("There are no dependent projects left to process.");
        }

        return projects;
    }

    protected Set<Artifact> getArtifactsFromReactor() {
        return reactorProjects.stream()
                .map(MavenProject::getArtifact)
                .filter(this::artifactMustExist)
                .collect(Collectors.toCollection(HashSet::new));
    }

    private Set<Artifact> getReactorProjectDependencies() throws MojoExecutionException {
        final Set<Artifact> dependencies = reactorProjects.stream()
                .flatMap(project -> project.getDependencyArtifacts().stream())
                .filter(artifact -> artifact.getScope().equals("compile"))
                .collect(Collectors.toSet());

        return resolveTransitiveArtifacts(dependencies).stream()
                .filter(this::artifactMustExist)
                .collect(Collectors.toCollection(HashSet::new));
    }

    /*
     * Resolves transitive dependencies for a set of given artifacts
     * Returns a Set of all artifacts resolved
     */
    private Set<Artifact> resolveTransitiveArtifacts(Set<Artifact> artifacts, ArtifactFilter filter)
            throws MojoExecutionException {
        try {
            ArtifactResolutionResult result = resolver.resolveTransitively(
                    artifacts, project.getArtifact(), localRepository, remoteRepositories, metadataSource, filter);
            return result.getArtifacts();
        } catch (ArtifactResolutionException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        } catch (ArtifactNotFoundException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private Set<Artifact> resolveTransitiveArtifacts(Set<Artifact> artifacts) throws MojoExecutionException {
        return resolveTransitiveArtifacts(artifacts, null);
    }

    /*
     * Returns the MavenProject for a given Artifact
     */
    private MavenProject getProjectForArtifact(Artifact artifact) throws MojoExecutionException {
        try {
            return projectBuilder.buildFromRepository(artifact, remoteRepositories, localRepository);
        } catch (InvalidProjectModelException e) {
            getLog().error("Validation Errors: " + e.getValidationResult().getMessages());
            throw new MojoExecutionException(e.getMessage(), e);
        } catch (ProjectBuildingException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private URL artifactToUrl(final Artifact artifact) {
        try {
            return artifact.getFile().toURI().toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean artifactMustExist(final Artifact input) {
        return input.getFile() != null && input.getFile().exists();
    }
}
