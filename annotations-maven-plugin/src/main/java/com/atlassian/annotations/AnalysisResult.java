package com.atlassian.annotations;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.concurrent.NotThreadSafe;

@NotThreadSafe
public class AnalysisResult {
    private final String clazz;
    private boolean unloadable = false;

    private final Map<Method, Set<Class<?>>> nonAnnotatedMethodReferences = new HashMap<>();

    public AnalysisResult(final String clazz) {
        this.clazz = clazz;
    }

    public String getClassName() {
        return clazz;
    }

    public boolean isUnloadable() {
        return unloadable;
    }

    public void setUnloadable() {
        this.unloadable = true;
    }

    public Map<Method, Set<Class<?>>> getNonAnnotatedMethodReferences() {
        return nonAnnotatedMethodReferences;
    }

    public void addUnannotatedMethodReference(Method method, Class<?> reference) {
        nonAnnotatedMethodReferences
                .computeIfAbsent(method, m -> new HashSet<>())
                .add(reference);
    }
}
