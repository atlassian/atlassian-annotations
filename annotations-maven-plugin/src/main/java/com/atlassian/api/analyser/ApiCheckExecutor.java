package com.atlassian.api.analyser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.atlassian.api.analyser.rules.NoAddAbstractMethodRule;
import com.atlassian.api.analyser.rules.NoAddExceptionRule;
import com.atlassian.api.analyser.rules.NoChangeProtectedFieldTypeRule;
import com.atlassian.api.analyser.rules.NoChangePublicFieldTypeRule;
import com.atlassian.api.analyser.rules.NoRemoveClassRule;
import com.atlassian.api.analyser.rules.NoRemoveExceptionRule;
import com.atlassian.api.analyser.rules.NoRemoveProtectedConcreteMethodRule;
import com.atlassian.api.analyser.rules.NoRemoveProtectedFieldRule;
import com.atlassian.api.analyser.rules.NoRemovePublicFieldRule;
import com.atlassian.api.analyser.rules.NoRemovePublicMethodRule;
import com.atlassian.api.analyser.rules.Rule;

class ApiCheckExecutor {
    private static final List<Rule> apiRules = new ArrayList<>();
    private static final List<Rule> spiRules = new ArrayList<>();

    static {
        apiRules.add(new NoRemoveClassRule());
        // Check for public -> downgrade
        apiRules.add(new NoRemovePublicMethodRule());
        apiRules.add(new NoRemovePublicFieldRule());
        apiRules.add(new NoChangePublicFieldTypeRule());
        apiRules.add(new NoAddExceptionRule());
    }

    static {
        spiRules.add(new NoRemoveClassRule());
        spiRules.add(new NoRemoveProtectedConcreteMethodRule());
        spiRules.add(new NoRemoveProtectedFieldRule());
        spiRules.add(new NoChangeProtectedFieldTypeRule());
        spiRules.add(new NoAddAbstractMethodRule());
        spiRules.add(new NoRemoveExceptionRule());
    }

    protected final String exclusionsXml;
    private final Log log;
    private final org.apache.maven.artifact.resolver.ArtifactResolver resolver;
    private final ArtifactFactory artifactFactory;
    protected final ArtifactRepository localRepository;
    private final org.apache.maven.project.MavenProject mavenProject;

    ApiCheckExecutor(CheckApiMojo mojo) {
        this.exclusionsXml = mojo.exclusionsXml;
        this.log = mojo.getLog();
        this.resolver = mojo.resolver;
        this.artifactFactory = mojo.artifactFactory;
        this.localRepository = mojo.localRepository;
        this.mavenProject = mojo.mavenProject;
    }

    public void checkBaselineXml(final String baselineXml) throws MojoExecutionException, MojoFailureException {

        Map<String, ApiClass> baseline = loadBaselineFromXML(baselineXml);
        checkBaseline(baselineXml, baseline);
    }

    public void checkBaselineArtifact(String comparisonVersion) throws MojoExecutionException, MojoFailureException {

        Map<String, ApiClass> baseline = loadBaselineFromArtifact(comparisonVersion);
        checkBaseline(comparisonVersion, baseline);
    }

    private void checkBaseline(String desc, Map<String, ApiClass> baseline)
            throws MojoExecutionException, MojoFailureException {
        Map<String, ApiClass> current = loadCurrent(desc);

        processExclusions(baseline);

        List<String> errors = new ArrayList<String>();

        for (Map.Entry<String, ApiClass> entry : baseline.entrySet()) {
            if (entry.getValue().isApi()) {
                checkApiClass(entry.getValue().getClassName(), baseline, current, errors);
            }
            if (entry.getValue().isSpi()) {
                checkSpiClass(entry.getValue().getClassName(), baseline, current, errors);
            }
            if (!entry.getValue().isApi()
                    && !entry.getValue().isSpi()
                    && !entry.getValue().isInternal()
                    && !entry.getValue().isExperimental()) {
                // Assume it is API
                checkApiClass(entry.getValue().getClassName(), baseline, current, errors);
            }
        }

        for (String error : errors) {
            log.error(error);
        }
        if (!errors.isEmpty()) {
            throw new MojoFailureException("API Checker has found compatibility " + errors.size()
                    + " errors, compared to version '" + desc + "'.");
        }
    }

    private void processExclusions(final Map<String, ApiClass> baseline) throws MojoExecutionException {
        // If present, we read the exclusions file and then remove from the baseline any classes or methods that are
        // excluded.
        if (exclusionsXml == null) {
            return;
        }

        SAXReader reader = new SAXReader();
        try {
            Document document = reader.read(exclusionsXml);
            Element root = document.getRootElement();
            // iterate through child elements of root
            for (Iterator i = root.elementIterator("class"); i.hasNext(); ) {
                Element classElement = (Element) i.next();
                Attribute exclude = classElement.attribute("exclude");
                if (exclude != null && exclude.getValue().equals("true")) {
                    // Remove the class from the base line
                    baseline.remove(classElement.attributeValue("name"));
                    log.debug("Excluded class " + classElement.attributeValue("name"));
                } else {
                    log.debug("Excluding class members for " + classElement.attributeValue("name"));
                    ApiClass apiClass = baseline.get(classElement.attributeValue("name"));
                    if (apiClass != null) {
                        // process methods
                        for (Iterator i2 = classElement.elementIterator("method"); i2.hasNext(); ) {
                            Element methodElement = (Element) i2.next();
                            Attribute methodExclude = methodElement.attribute("exclude");
                            if (methodExclude != null
                                    && methodExclude.getValue().equals("true")) {
                                ApiMethod method = new ApiMethod(
                                        classElement.attributeValue("name"),
                                        methodElement.attributeValue("name"),
                                        methodElement.attributeValue("desc"));
                                apiClass.getMethods().remove(method);
                                log.debug("Excluded method " + methodElement.attributeValue("name"));
                            }
                        }
                        // process fields
                        for (Iterator i3 = classElement.elementIterator("field"); i3.hasNext(); ) {
                            Element fieldElement = (Element) i3.next();
                            Attribute fieldExclude = fieldElement.attribute("exclude");
                            if (fieldExclude != null && fieldExclude.getValue().equals("true")) {
                                ApiField field = new ApiField(
                                        classElement.attributeValue("name"), fieldElement.attributeValue("name"));
                                apiClass.getFields().remove(field);
                                log.debug("Excluded field " + fieldElement.attributeValue("name"));
                            }
                        }
                    }
                }
            }
        } catch (DocumentException e) {
            throw new MojoExecutionException("Can't read exclusions XML from: " + exclusionsXml, e);
        }
    }

    private Map<String, ApiClass> loadCurrent(String desc) {
        Artifact currentArtifact = mavenProject.getArtifact();
        File currentJarFile = currentArtifact.getFile();
        log.info("Checking API of " + currentJarFile + " against " + desc);
        Document documentx = loadApiJar(currentJarFile);
        return loadClassMap(documentx);
    }

    private Map<String, ApiClass> loadBaselineFromXML(final String baselineXml) throws MojoExecutionException {
        SAXReader reader = new SAXReader();
        try {
            Document document = reader.read(baselineXml);
            return loadClassMap(document);
        } catch (DocumentException e) {
            throw new MojoExecutionException("Can't read baseline XML from: " + baselineXml, e);
        }
    }

    private Map<String, ApiClass> loadBaselineFromArtifact(String comparisonVersion) throws MojoExecutionException {
        Artifact currentArtifact = mavenProject.getArtifact();
        Artifact baselineArtifact = artifactFactory.createArtifact(
                mavenProject.getGroupId(),
                mavenProject.getArtifactId(),
                comparisonVersion,
                "",
                currentArtifact.getType());
        try {
            resolver.resolve(baselineArtifact, mavenProject.getRemoteArtifactRepositories(), localRepository);
        } catch (ArtifactResolutionException e) {
            throw new MojoExecutionException("Artifact not found: " + baselineArtifact, e);
        } catch (ArtifactNotFoundException e) {
            throw new MojoExecutionException("Artifact not found: " + baselineArtifact, e);
        }

        File baselineJarFile = baselineArtifact.getFile();
        Document document = loadApiJar(baselineJarFile);
        return loadClassMap(document);
    }

    private void checkApiClass(
            String className, Map<String, ApiClass> baseline, Map<String, ApiClass> current, List<String> errors) {
        checkRuleSet(className, baseline, current, errors, apiRules);
    }

    private void checkSpiClass(
            String className, Map<String, ApiClass> baseline, Map<String, ApiClass> current, List<String> errors) {
        checkRuleSet(className, baseline, current, errors, spiRules);
    }

    private void checkRuleSet(
            String className,
            Map<String, ApiClass> baseline,
            Map<String, ApiClass> current,
            List<String> errors,
            List<Rule> rules) {
        for (Rule rule : rules) {
            rule.apply(className, baseline, current, errors);
        }
    }

    private Map<String, ApiClass> loadClassMap(Document document) {
        Map<String, ApiClass> classes = new HashMap<String, ApiClass>();

        Element root = document.getRootElement();
        // iterate through child elements of root
        for (Iterator i = root.elementIterator("class"); i.hasNext(); ) {
            Element element = (Element) i.next();
            ApiClass apiClass = new ApiClass(element.attributeValue("name"));
            classes.put(element.attributeValue("name"), apiClass);
            apiClass.setSpi(element.attribute("spi") != null);
            apiClass.setApi(element.attribute("api") != null);
            apiClass.setInternal(element.attribute("internal") != null);
            apiClass.setExperimental(element.attribute("experimental") != null);
            apiClass.setAbstractClass(element.attribute("abstract") != null);
            apiClass.setPublicClass(element.attribute("public") != null);

            loadClassMethods(element, apiClass);
            loadClassFields(element, apiClass);
        }

        return classes;
    }

    private void loadClassMethods(Element classElement, ApiClass apiClass) {
        for (Iterator i = classElement.elementIterator("method"); i.hasNext(); ) {
            Element element = (Element) i.next();
            ApiMethod apiMethod = new ApiMethod(
                    apiClass.getClassName(), element.attributeValue("name"), element.attributeValue("desc"));
            apiMethod.setAbstractMethod(element.attribute("abstract") != null);
            apiMethod.setAccess(element.attributeValue("access"));
            apiMethod.setDeprecated(element.attributeValue("deprecated") != null);
            apiMethod.setInternal(element.attribute("internal") != null);
            apiMethod.setExperimental(element.attribute("experimental") != null);
            apiMethod.setDeprecated(element.attributeValue("deprecated") != null);
            apiClass.addMethod(apiMethod);

            loadMethodExceptions(element, apiMethod);
        }
    }

    private void loadMethodExceptions(Element methodElement, ApiMethod apiMethod) {
        for (Iterator i = methodElement.elementIterator("exception"); i.hasNext(); ) {
            Element element = (Element) i.next();
            loadMethodExceptions(element, apiMethod);
            apiMethod.addException(element.attributeValue("name"));
        }
    }

    private void loadClassFields(Element classElement, ApiClass apiClass) {
        for (Iterator i = classElement.elementIterator("field"); i.hasNext(); ) {
            Element element = (Element) i.next();
            ApiField apiField = new ApiField(apiClass.getClassName(), element.attributeValue("name"));
            apiField.setDesc(element.attributeValue("desc"));
            apiField.setAccess(element.attributeValue("access"));
            apiClass.addField(apiField);
        }
    }

    private Document loadApiJar(File jarFile) {
        Document document = DocumentHelper.createDocument();
        Element productElement = document.addElement("product");

        ProductApiJarScanner scanner = new ProductApiJarScanner();
        try {
            scanner.scanJar(productElement, jarFile.getAbsolutePath(), new FileInputStream(jarFile), log);
        } catch (FileNotFoundException ex) {
            log.error("Error opening jar file: " + jarFile, ex);
        }
        return document;
    }
}
