package com.atlassian.api.analyser.rules;

import java.util.List;
import java.util.Map;

import com.atlassian.api.analyser.ApiClass;

/**
 * A rule to be applied to a API class to test whether we have boken our API contract.
 *
 * @since v5.0
 */
public interface Rule {
    void apply(String className, Map<String, ApiClass> baseLine, Map<String, ApiClass> current, List<String> errors);
}
