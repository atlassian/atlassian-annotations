package com.atlassian.api.analyser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 * This class exports the API definition of the current artifact as an XML file
 * <p/>
 * This XML signature file can be used as a baseline to check for API/SPI breakages in later versions of the artifact.
 * <p/>
 * This uses ASM to process the classes in the jar file.
 */
@Mojo(name = "extract-api", defaultPhase = LifecyclePhase.VERIFY)
public class ExtractApiMojo extends AbstractMojo {

    @Component
    private org.apache.maven.artifact.resolver.ArtifactResolver resolver;

    @Component
    private ArtifactFactory artifactFactory;

    /**
     * Location of the local repository.
     */
    @Parameter(property = "localRepository", readonly = true, required = true)
    protected ArtifactRepository localRepository;

    /**
     * Location of the local repository.
     */
    @Parameter(property = "checkApi.baselineXML", required = true)
    protected String baselineXml;

    /**
     */
    @Parameter(defaultValue = "${project}")
    private MavenProject mavenProject;

    public void execute() throws MojoExecutionException, MojoFailureException {

        Artifact currentArtifact = mavenProject.getArtifact();
        File currentJarFile = currentArtifact.getFile();
        Document document = loadApiJar(currentJarFile);

        // If all went well we can output the document
        // Pretty print the document to System.out
        final OutputFormat format = OutputFormat.createPrettyPrint();
        try (Writer out = new FileWriter(baselineXml)) {
            final XMLWriter writer = new XMLWriter(out, format);
            writer.write(document);
            writer.close();
        } catch (IOException e) {
            throw new MojoExecutionException("Error Writing XML document: " + baselineXml, e);
        }
    }

    private Document loadApiJar(File jarFile) {
        Document document = DocumentHelper.createDocument();
        Element productElement = document.addElement("product");

        ProductApiJarScanner scanner = new ProductApiJarScanner();
        try {
            scanner.scanJar(productElement, jarFile.getAbsolutePath(), new FileInputStream(jarFile), getLog());
        } catch (FileNotFoundException ex) {
            getLog().error("Error opening jar file: " + jarFile, ex);
        }
        return document;
    }
}
