package com.atlassian.api.analyser;

/**
 */
public class ApiField {
    private final String className;
    private final String fieldName;
    private String desc;
    private Access access;

    public ApiField(final String className, String fieldName) {
        this.className = className;
        this.fieldName = fieldName;
    }

    public String getClassName() {
        return className;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Access getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = Access.valueOf(access);
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApiField)) {
            return false;
        }

        ApiField apiField = (ApiField) o;

        if (className != null ? !className.equals(apiField.className) : apiField.className != null) {
            return false;
        }
        if (fieldName != null ? !fieldName.equals(apiField.fieldName) : apiField.fieldName != null) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int result = className != null ? className.hashCode() : 0;
        result = 31 * result + (fieldName != null ? fieldName.hashCode() : 0);
        return result;
    }

    public boolean matches(final ApiField member) {
        return equals(member);
    }
}
