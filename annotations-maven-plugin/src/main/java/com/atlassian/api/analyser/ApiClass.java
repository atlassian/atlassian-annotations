package com.atlassian.api.analyser;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class ApiClass {
    private final String className;
    private final Map<ApiMethod, ApiMethod> methods = new HashMap<ApiMethod, ApiMethod>();
    private final Map<ApiField, ApiField> fields = new HashMap<ApiField, ApiField>();
    private boolean api;
    private boolean spi;
    private boolean internal;
    private boolean experimental;
    private boolean abstractClass;
    private boolean publicClass;

    public ApiClass(final String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public boolean isApi() {
        return api;
    }

    public void setApi(boolean api) {
        this.api = api;
    }

    public boolean isSpi() {
        return spi;
    }

    public void setSpi(boolean spi) {
        this.spi = spi;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public boolean isExperimental() {
        return experimental;
    }

    public void setExperimental(boolean experimental) {
        this.experimental = experimental;
    }

    public boolean isAbstractClass() {
        return abstractClass;
    }

    public void setAbstractClass(boolean abstractClass) {
        this.abstractClass = abstractClass;
    }

    public boolean isPublicClass() {
        return publicClass;
    }

    public void setPublicClass(boolean publicClass) {
        this.publicClass = publicClass;
    }

    public Map<ApiMethod, ApiMethod> getMethods() {
        return methods;
    }

    public void addMethod(ApiMethod apiMethod) {
        getMethods().put(apiMethod, apiMethod);
    }

    public boolean containsMethod(ApiMethod o) {
        return methods.containsKey(o);
    }

    public Map<ApiField, ApiField> getFields() {
        return fields;
    }

    public void addField(ApiField apiField) {
        fields.put(apiField, apiField);
    }

    public boolean containsField(ApiField o) {
        return fields.containsKey(o);
    }

    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApiClass)) {
            return false;
        }

        final ApiClass apiClass = (ApiClass) o;

        if (className != null ? !className.equals(apiClass.className) : apiClass.className != null) {
            return false;
        }

        return true;
    }

    public String getShortName() {
        return "Class Type Usages";
    }

    public int hashCode() {
        return className != null ? className.hashCode() : 0;
    }

    public String toString() {
        return className;
    }

    public boolean matches(ApiClass member) {
        return this.equals(member);
    }
}
