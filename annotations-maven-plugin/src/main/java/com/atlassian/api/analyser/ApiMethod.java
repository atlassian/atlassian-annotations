package com.atlassian.api.analyser;

import java.util.ArrayList;
import java.util.Collection;

/**
 */
public class ApiMethod {
    private final String className;
    private final String methodName;
    private final String desc;
    private Collection<String> exceptionNames = new ArrayList<String>();
    private boolean abstractMethod;
    private boolean deprecated;
    private Access access;
    private boolean internal;
    private boolean experimental;

    public ApiMethod(final String className, final String methodName, final String desc) {
        this.className = className;
        this.methodName = methodName;
        this.desc = desc;
    }

    public String getClassName() {
        return className;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getDesc() {
        return desc;
    }

    public Collection<String> getExceptionNames() {
        return exceptionNames;
    }

    public void setExceptionNames(Collection<String> exceptionNames) {
        this.exceptionNames = exceptionNames;
    }

    public boolean isAbstractMethod() {
        return abstractMethod;
    }

    public void setAbstractMethod(boolean abstractMethod) {
        this.abstractMethod = abstractMethod;
    }

    public Access getAccess() {
        return access;
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    public void setAccess(String access) {
        this.access = Access.valueOf(access);
    }

    public boolean isDeprecated() {
        return deprecated;
    }

    public void setDeprecated(boolean deprecated) {
        this.deprecated = deprecated;
    }

    public boolean addException(String s) {
        return exceptionNames.add(s);
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public boolean isExperimental() {
        return experimental;
    }

    public void setExperimental(boolean experimental) {
        this.experimental = experimental;
    }

    /**
     * Two methods are really equal if their parameters match.  The return code is irrelevant.
     */
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApiMethod)) {
            return false;
        }

        ApiMethod apiMethod = (ApiMethod) o;

        if (className != null ? !className.equals(apiMethod.className) : apiMethod.className != null) {
            return false;
        }
        if (desc != null ? !desc.equals(apiMethod.desc) : apiMethod.desc != null) {
            return false;
        }
        if (methodName != null ? !methodName.equals(apiMethod.methodName) : apiMethod.methodName != null) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int result = className != null ? className.hashCode() : 0;
        result = 31 * result + (methodName != null ? methodName.hashCode() : 0);
        result = 31 * result + (desc != null ? desc.hashCode() : 0);
        return result;
    }

    public String getMethodSignature() {
        return methodName + desc;
    }
}
