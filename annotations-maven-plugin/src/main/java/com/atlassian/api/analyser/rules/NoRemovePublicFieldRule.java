package com.atlassian.api.analyser.rules;

import java.util.List;
import java.util.Map;

import com.atlassian.api.analyser.Access;
import com.atlassian.api.analyser.ApiClass;
import com.atlassian.api.analyser.ApiField;

/**
 * You are not allowed to remove public fields from an api class.
 *
 * @since v5.0
 */
public class NoRemovePublicFieldRule implements Rule {

    public void apply(
            String className, Map<String, ApiClass> baseLine, Map<String, ApiClass> current, List<String> errors) {
        // For the class in the baseline ensure all public fields are also present in the current;
        ApiClass currentClass = current.get(className);
        ApiClass baseLineClass = baseLine.get(className);
        if (currentClass == null || baseLineClass == null) {
            // This is not our problem.  Another rule will check this if needs be.
            return;
        }

        for (ApiField baselineField : baseLineClass.getFields().keySet()) {
            if (baselineField.getAccess() == Access.PUBLIC) {
                ApiField currentField = currentClass.getFields().get(baselineField);
                if (currentField == null) {
                    errors.add("Class - '" + className + "' : Public field '" + baselineField.getFieldName()
                            + "' removed from class.");
                } else if (currentField.getAccess() != Access.PUBLIC) {
                    errors.add("Class - '" + className + "' : Accessibility of public field '"
                            + baselineField.getFieldName() + "' has been weakened.");
                }
            }
        }
    }
}
