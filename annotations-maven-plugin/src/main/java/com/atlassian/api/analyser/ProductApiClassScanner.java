package com.atlassian.api.analyser;

import org.apache.xbean.asm9.AnnotationVisitor;
import org.apache.xbean.asm9.Attribute;
import org.apache.xbean.asm9.ClassVisitor;
import org.apache.xbean.asm9.FieldVisitor;
import org.apache.xbean.asm9.MethodVisitor;
import org.apache.xbean.asm9.Opcodes;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;

/**
 */
public class ProductApiClassScanner extends ClassVisitor {
    private final Element product;
    private Element classNode;

    public ProductApiClassScanner(final Element product) {
        super(Opcodes.ASM9);
        this.product = product;
    }

    public void visit(
            final int version,
            final int access,
            final String name,
            final String signature,
            final String superName,
            final String[] interfaces) {
        classNode = new DOMElement("class");
        classNode.addAttribute("name", name);
        classNode.addAttribute("signature", signature);
        if ((access & Opcodes.ACC_PUBLIC) == Opcodes.ACC_PUBLIC) classNode.addAttribute("public", "true");
        if ((access & Opcodes.ACC_INTERFACE) == Opcodes.ACC_INTERFACE) classNode.addAttribute("interface", "true");
        if ((access & Opcodes.ACC_ABSTRACT) == Opcodes.ACC_ABSTRACT) classNode.addAttribute("abstract", "true");
    }

    public void visitSource(final String source, final String debug) {}

    public void visitOuterClass(final String owner, final String name, final String desc) {}

    public AnnotationVisitor visitAnnotation(final String desc, final boolean visible) {
        // We want to know about 2 annotations @PublicApi, @PublicSpi and @Internal
        if (desc.equals("Lcom/atlassian/annotations/PublicApi;")) classNode.addAttribute("api", "true");
        if (desc.equals("Lcom/atlassian/annotations/PublicSpi;")) classNode.addAttribute("spi", "true");
        if (desc.equals("Lcom/atlassian/annotations/Internal;")) classNode.addAttribute("internal", "true");
        if (desc.equals("Lcom/atlassian/annotations/ExperimentalApi;")) classNode.addAttribute("experimental", "true");
        return null;
    }

    public void visitAttribute(final Attribute attr) {}

    public void visitInnerClass(final String name, final String outerName, final String innerName, final int access) {}

    public FieldVisitor visitField(
            final int access, final String name, final String desc, final String signature, final Object value) {
        if ((access & Opcodes.ACC_PUBLIC) == Opcodes.ACC_PUBLIC) {
            Element fieldElement = classNode.addElement("field");
            fieldElement.addAttribute("name", name);
            fieldElement.addAttribute("desc", desc);
            fieldElement.addAttribute("access", "PUBLIC");
        }
        return null;
    }

    public MethodVisitor visitMethod(
            final int access, final String name, final String desc, final String signature, final String[] exceptions) {
        if ((access & Opcodes.ACC_PUBLIC) == Opcodes.ACC_PUBLIC
                || (access & Opcodes.ACC_PROTECTED) == Opcodes.ACC_PROTECTED) {
            Element methodElement = classNode.addElement("method");
            methodElement.addAttribute("name", name);
            methodElement.addAttribute("desc", desc);
            if ((access & Opcodes.ACC_PUBLIC) == Opcodes.ACC_PUBLIC) methodElement.addAttribute("access", "PUBLIC");
            else if ((access & Opcodes.ACC_PROTECTED) == Opcodes.ACC_PROTECTED)
                methodElement.addAttribute("access", "PROTECTED");
            else if ((access & Opcodes.ACC_PRIVATE) == Opcodes.ACC_PRIVATE)
                methodElement.addAttribute("access", "PRIVATE");
            else methodElement.addAttribute("access", "PACKAGE");
            if ((access & Opcodes.ACC_ABSTRACT) == Opcodes.ACC_ABSTRACT) methodElement.addAttribute("abstract", "true");
            if ((access & Opcodes.ACC_DEPRECATED) == Opcodes.ACC_DEPRECATED)
                methodElement.addAttribute("deprecated", "true");
            if (exceptions != null) {
                for (String exception : exceptions) {
                    Element element = methodElement.addElement("exception");
                    element.addAttribute("name", exception);
                }
            }
            return new ProductApiMethodScanner(methodElement);
        }
        return null;
    }

    public void visitEnd() {
        if (classNode != null && classNode.attribute("public") != null) {
            product.add(classNode);
        }
    }
}
