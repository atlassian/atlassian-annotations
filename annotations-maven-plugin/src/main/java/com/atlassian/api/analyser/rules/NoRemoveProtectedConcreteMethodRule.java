package com.atlassian.api.analyser.rules;

import java.util.List;
import java.util.Map;

import com.atlassian.api.analyser.Access;
import com.atlassian.api.analyser.ApiClass;
import com.atlassian.api.analyser.ApiMethod;

/**
 * You are not allowed to remove protected or public concrete methods from an spi abstract class.
 *
 * @since v5.0
 */
public class NoRemoveProtectedConcreteMethodRule implements Rule {

    public void apply(
            String className, Map<String, ApiClass> baseLine, Map<String, ApiClass> current, List<String> errors) {
        // For the class in the baseline ensure all public methods are also present in the current;
        ApiClass currentClass = current.get(className);
        ApiClass baseLineClass = baseLine.get(className);
        if (currentClass == null || baseLineClass == null) {
            // This is not our problem.  Another rule will check this if needs be.
            return;
        }

        for (ApiMethod apiMethod : baseLineClass.getMethods().keySet()) {
            if (apiMethod.getAccess() == Access.PUBLIC || apiMethod.getAccess() == Access.PROTECTED) {
                if (!currentClass.getMethods().containsKey(apiMethod)) {
                    errors.add("Class - '" + className + "' : '" + apiMethod.getAccess() + " method '"
                            + apiMethod.getMethodSignature() + "' removed from class.");
                }
            }
        }
    }
}
