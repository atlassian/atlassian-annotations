package com.atlassian.api.analyser.rules;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.atlassian.api.analyser.ApiClass;
import com.atlassian.api.analyser.ApiMethod;

/**
 * You are not allowed to add exceptions to a methods of an spi class or interface.
 * Interface methods are always in fact abstract.
 *
 * @since v5.0
 */
public class NoAddExceptionRule implements Rule {

    public void apply(
            String className, Map<String, ApiClass> baseLine, Map<String, ApiClass> current, List<String> errors) {
        ApiClass currentClass = current.get(className);
        ApiClass baseLineClass = baseLine.get(className);
        if (currentClass == null || baseLineClass == null) {
            // This is not our problem.  Another rule will check this if needs be.
            return;
        }

        for (ApiMethod baselineMethod : baseLineClass.getMethods().keySet()) {
            if (baselineMethod.isAbstractMethod()) {
                ApiMethod currentMethod = baseLineClass.getMethods().get(baselineMethod);
                checkNoAddedException(className, baselineMethod, currentMethod, errors);
            }
        }
    }

    private void checkNoAddedException(
            String className, ApiMethod baselineMethod, ApiMethod currentMethod, List<String> errors) {
        if (currentMethod == null) {
            // This is not our problem.  Another rule will check this if needs be.
            return;
        }
        // For the method in the baseline api ensure all exceptions are also present in the current;
        Collection<String> currentExceptions = currentMethod.getExceptionNames();
        Collection<String> baselineExceptions = baselineMethod.getExceptionNames();

        if (currentExceptions == null) {
            return;
        }
        for (String currentException : currentExceptions) {
            if (baselineExceptions == null || !(baselineExceptions.contains(currentException))) {
                errors.add("Class - '" + className + "' : Exception '" + currentException + "' added to method '"
                        + currentMethod.getMethodSignature() + "'.");
            }
        }
    }
}
