package com.atlassian.api.analyser;

/**
 */
public class ApiProduct {
    private final String product;
    private final String productVersion;

    public ApiProduct(final String product, final String productVersion) {
        this.product = product;
        this.productVersion = productVersion;
    }

    public String getProduct() {
        return product;
    }

    public String getProductVersion() {
        return productVersion;
    }

    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApiProduct)) {
            return false;
        }

        final ApiProduct that = (ApiProduct) o;

        if (product != null ? !product.equals(that.product) : that.product != null) {
            return false;
        }
        if (productVersion != null ? !productVersion.equals(that.productVersion) : that.productVersion != null) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int result = product != null ? product.hashCode() : 0;
        result = 31 * result + (productVersion != null ? productVersion.hashCode() : 0);
        return result;
    }
}
