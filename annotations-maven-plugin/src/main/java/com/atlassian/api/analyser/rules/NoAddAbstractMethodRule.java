package com.atlassian.api.analyser.rules;

import java.util.List;
import java.util.Map;

import com.atlassian.api.analyser.ApiClass;
import com.atlassian.api.analyser.ApiMethod;

/**
 * You are not allowed to add abstract methods to an spi class or interface.
 * Interface methods are always in fact abstract.
 *
 * @since v5.0
 */
public class NoAddAbstractMethodRule implements Rule {

    public void apply(
            String className, Map<String, ApiClass> baseLine, Map<String, ApiClass> current, List<String> errors) {
        // For the class in the current api ensure all methods are also present in the baseLine;
        ApiClass currentClass = current.get(className);
        ApiClass baseLineClass = baseLine.get(className);
        if (currentClass == null || baseLineClass == null) {
            // This is not our problem.  Another rule will check this if needs be.
            return;
        }

        for (ApiMethod apiMethod : currentClass.getMethods().keySet()) {
            if (apiMethod.isAbstractMethod()) {
                if (!baseLineClass.getMethods().containsKey(apiMethod)) {
                    errors.add("Class - '" + className + "' : Method '" + apiMethod.getMethodSignature()
                            + "' added to class.");
                }
            }
        }
    }
}
